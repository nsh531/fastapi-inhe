// יצירת מודול האפליקציה
var app = angular.module('fastAPIApp', []);

// בקר ראשי
app.controller('MainController', function($scope) {
      // רשימת הנושאים במדריך
        $scope.topics = [
                {name: 'התקנה', file: 'topics/installation.html'},
                    {name: 'יישום בסיסי', file: 'topics/basic-app.html'},
                        {name: 'הרצת היישום', file: 'topics/running-app.html'},
                            {name: 'כתובת היישום', file: 'topics/app-url.html'},
                                {name: 'הוספת ניתובים', file: 'topics/add-routes.html'},
                                    {name: 'מידע נוסף', file: 'topics/more-info.html'}
        ];

          // פונקציה להצגת תוכן של נושא
            $scope.showTopic = function(topicFile) {
                    // קוד להצגת התוכן של הנושא המבוקש
                        // למשל, טעינת התוכן מהקובץ ההתאם והצגתו בחלון מודאלי
            }
});
            }
        ]
})